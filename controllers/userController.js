const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth")


/* User Registration */
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		first: reqBody.first,
		last: reqBody.last,
		email: reqBody.email,
		address: reqBody.address,
		mobile: reqBody.mobile,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return User.find({email:reqBody.email}).then( result => {

		/* FEATURE TO CHECK IF EMAIL IS ALREADY TAKEN */
		if(result.length > 0) {
			return (`EMAIL IS ALREADY TAKEN, PLEASE USE ANOTHER EMAIL ADDRESS`)

		} else {
			return newUser.save().then((user, error) => {

				if(error) {
					return false
				} else {
					return user
				}
			})

		}
	})
}


/* User Login */
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		// console.log(result)

		if (result == null){

			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				console.log(result)
				return{AccessToken: auth.createAccessToken(result)}

			} else {
				return false
			}
		}
	})


}


// Retrieve user details (For Frontend)
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


/* CREATE ORDER */
module.exports.orderProduct = async (data) => {

	let orderDetails = await Product.findById(data.productId).then(result => {
		return result;
	}) 

	console.log(orderDetails)
	console.log(orderDetails.price)
	console.log(typeof orderDetails.price)

	let isUserOrder = await User.findById(data.userId).then(user => {
		

		user.orders.push(
			{
			products: [{
				productName: orderDetails.name,
				quantity: data.quantity
	
			}],
			totalAmount: orderDetails.price * data.quantity

		}); 

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}

		})
	})

	let isProductOrder = await Product.findById(data.productId).then(product => {
		product.orders.push({orderId: data.userId});
		return product.save().then((product, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	if(isUserOrder && isProductOrder){
		return true
	} else {
		return false
	}

}





/* TO CHECK USER DETAILS */
module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		result.password = "***********";
		return result;
	})
}


/* STRETCH GOALS */
/* SET USER AS ADMIN (ADMIN ONLY) */
module.exports.setAsAdmin = (reqParams, reqBody) => {

	let setAsAdmin = {
	isAdmin: reqBody.isAdmin
	};
	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((user, error) => {
		if (error){
			return false;
		} else {
			return ("USER HAS BEEN SET TO ADMIN");
		}
	});
}

/* RETRIEVE AUTHENTICATED USER'S ORDERS */
module.exports.myOrders = (reqParams) => {


	return User.findByIdAndUpdate(reqParams.userId).then(result => {
	
	let myOrders = result.orders
	if(myOrders.length > 0){
	return result.orders}
	else {
		return("NO ORDERS PLACED")
	}

	})
}

/* RETRIEVING ALL ORDERS (ADMIN ONLY) */
/* Version 1 */
module.exports.allOrdersV1 = () => {
	return Product.find({}).then((result, response) => {

	let orderList = []	
	for (let i=0; i<=result.length+1; i++){ 
		if(i<result.length){
		orderList[i] = result[i].orders

		
	}
	} return orderList

})}

/* Version 1*/
module.exports.allOrdersV2 = () => {
	return Product.find({}).then((result, response) => {

	let orderList = []	
	for (let i=0; i<=result.length+1; i++){ 
		if(i<result.length){
		orderList[i] = result[i].orders.length + " " + result[i].name

		
	}
	} return orderList

})}

/* CART */
module.exports.myCart = async (data) => {

	let orderDetails = await Product.findById(data.productId).then(result => {
		return result;
	}) 

	console.log(orderDetails)
	console.log(orderDetails.price)
	console.log(typeof orderDetails.price)

	let isUserOrder = await User.findById(data.userId).then(user => {
		

		user.cart.push(
			{
			products: [{
				productName: orderDetails.name,
				quantity: data.quantity
	
			}],
			totalAmount: orderDetails.price * data.quantity

		}); 

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}

		})
	})

	let isProductOrder = await Product.findById(data.productId).then(product => {
		product.orders.push({orderId: data.userId});
		return product.save().then((product, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	if(isUserOrder && isProductOrder){
		return true
	} else {
		return false
	}

}



/* CHANGE PASSWORD */
module.exports.changePassword = (reqParams, reqBody) => {

	let changePassword = {
	password: bcrypt.hashSync(reqBody.password, 10)
	};
	return User.findByIdAndUpdate(reqParams.userId, changePassword).then((user, error) => {
		if (error){
			return false;
		} else {
			return ("PASSWORD HAS BEEN CHANGED");
		}
	});
}

/* RETRIEVE ALL USERS */
module.exports.allUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
};