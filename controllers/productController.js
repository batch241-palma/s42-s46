const Product = require("../models/Product");

/* ADDING PRODUCTS */
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}) 

	return newProduct.save().then((product, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
};

/* RETRIEVING ALL ACTIVE PRODUCTS */
module.exports.activeProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};

/* RETRIEVING ALL PRODUCTS (ADMIN ONLY) */
module.exports.allProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
};

/* RETRIEVE SINGLE PRODUCT */
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

/* UPDATE A PRODUCT (ADMIN ONLY) */
module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	});
};

/* ARCHIVING A PRODUCT (ADMIN ONLY) */
module.exports.archiveProduct = (reqParams, reqBody) => {
	let archiveProduct = {
		isActive: reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((product, error) => {
		if (error){
			return false;
		} else {
			return ("PRODUCT ARCHIVED");
		}
	});
};




