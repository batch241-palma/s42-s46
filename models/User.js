const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	first: {
		type: String,
		required: [true, "First Name is required"]
	},

	last: {
		type: String,
		required: [true, "Last Name is required"]
	},


	email: {
		type: String,
		required: [true, "Email is required"]
	},

	address: {
		type: String,
		required: [true, "Email is required"]
	},

	mobile: {
		type: Number,
		required: [true, "Mobile is required"]
	},		

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [

		{
			products: [{
				productName: {
				type: String
				},
				quantity: {
				type: Number
				},
				
			}],
			totalAmount: {
				type: Number
			},
			purchasedOn: {
				type: Date,
				default: new Date()			
			}
		}	

	],

/* Stretch Goal CART */

	cart: [

		{
			AddedProducts: {
				type: String
			},
			CartQuantity: {
				type: Number
			},

			SubtotalForEachItem: {
				type: Number
			},
			TotalPriceForAllItems: {
				type: Number
			}
		}

	]

})






module.exports = mongoose.model("User", userSchema);