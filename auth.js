const jwt = require("jsonwebtoken");

const secret = "Capstone2API";

/* FOR TOKEN CREATION */
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
}

/* To check wether user is admin or not */
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;
	if(typeof token !== "undefined") {
		console.log(token);

		token = token.slice(7, token.length);
		console.log(token)
		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return res.send({authentication: "Invalid Token"});
			} else {
				next();
			}
		})
	} else {
		return res.send({auth: "failed"});
	}
}

module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		
		token = token.slice(7,token.length);

		return jwt.verify(token, secret, (error, data) => {

			if(error) {
				return("Invalid Token")
			} else {
				return jwt.decode(token, {complete: true}).payload;
				console.log(token);
			}
		})
	} else {
		return("Invalid Token")
	}
}