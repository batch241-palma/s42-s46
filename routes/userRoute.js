const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


/* User Registration */
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});

/* User Login */
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Retreive user details (For Frontend)
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


/* CREATE ORDER */

router.post("/order", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	} 

	if(userData.isAdmin == false){

	userController.orderProduct(data).then(resultFromController => res.send(
		resultFromController));
	
	} else {
		return res.send("ONLY USERS CAN PLACE AN ORDER")
	}

});



/* TO CHECK USER DETAILS */
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(req.body.id == userData.id) {
		userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));
	} else {
		return false
	}
});


/* STRETCH GOALS */
/* SET USER AS ADMIN (ADMIN ONLY) */
router.patch("/:userId/setAsAdmin", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true){

	userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
	
	} else {
		return res.send("YOU DON'T HAVE ADMIN ACCESS")
	}
})

/* RETRIEVE AUTHENTICATED USER'S ORDERS */
router.get("/:userId/myOrders", (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.myOrders(req.params, req.body).then(resultFromController => res.send(resultFromController));

})


/* RETRIEVING ALL ORDERS (ADMIN ONLY) */
router.get("/allOrdersV1", (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){

	userController.allOrdersV1(req.body).then(resultFromController => res.send(resultFromController));

	} else {
		return res.send("ONLY ADMIN CAN RETRIEVE ALL ORDERS")
	}
});


router.get("/allOrdersV2", (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){

	userController.allOrdersV2(req.body).then(resultFromController => res.send(resultFromController));

	} else {
		return res.send("ONLY ADMIN CAN RETRIEVE ALL ORDERS")
	}
});


/* CART */
router.post("/cart", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	} 

	if(userData.isAdmin == false){

	userController.myCart(data).then(resultFromController => res.send(
		resultFromController));
	
	} else {
		return res.send("ONLY USERS CAN PLACE AN ORDER")
	}

});


/* CHANGE PASSWORD */
router.patch("/:userId/changePassword", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true){

	userController.changePassword(req.params, req.body).then(resultFromController => res.send(resultFromController));
	
	} else {
		return res.send("ONLY USER CAN CHANGE PASSWORD")
	}
})


module.exports = router;

/* RETRIEVE ALL USERS */
router.get("/all", (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// if(isAdmin === true){

	userController.allUsers(req.body).then(resultFromController => res.send(resultFromController));

	// } 
	// else {
	// 	return res.send("ONLY ADMIN CAN RETRIEVE ALL PRODUCT")
	// }
});