const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");


/* ADDING PRODUCTS */
router.post("/add", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){

		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send("ONLY ADMIN CAN ADD PRODUCT")
	}
});

/* RETRIEVING ALL ACTIVE PRODUCTS */
router.get("/active", (req, res) => {
	console.log(res)
	productController.activeProducts(req.body).then(resultFromController => res.send(resultFromController));
});

/* RETRIEVING ALL PRODUCTS (ADMIN ONLY) */
router.get("/all", (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// if(isAdmin === true){

	productController.allProducts(req.body).then(resultFromController => res.send(resultFromController));

	// } 
	// else {
	// 	return res.send("ONLY ADMIN CAN RETRIEVE ALL PRODUCT")
	// }
});

/* RETRIEVE SINGLE PRODUCT */
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


/* UPDATE A PRODUCT (ADMIN ONLY) */
router.put("/:productId", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send("ONLY ADMIN CAN UPDATE A PRODUCT")
	}
});


/* ARCHIVING A PRODUCT (ADMIN ONLY) */
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send("ONLY ADMIN CAN ARCHIVE ALL PRODUCT")
	}
})


/* For front active-inactive*/
router.put("/:productId", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send("ONLY ADMIN CAN UPDATE A PRODUCT")
	}
});


module.exports = router;