const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");

const port = 4000;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.set('strictQuery', true);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.z41cdv5.mongodb.net/Capstone-2-Palma?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Server is now connected"));


app.use("/users", userRoute);
app.use("/products", productRoute);





app.listen(port, () => {
	console.log(`API is now online at port ${port}`);
})